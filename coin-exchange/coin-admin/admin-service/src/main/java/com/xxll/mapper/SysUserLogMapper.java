package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.SysUserLog;

public interface SysUserLogMapper extends BaseMapper<SysUserLog> {
}