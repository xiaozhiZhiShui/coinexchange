package com.xxll.service;

import com.xxll.domain.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface SysMenuService extends IService<SysMenu>{

    /**
     * 通过用户的id 查询用户的菜单数据
     * @param userId
     **/
    List<SysMenu> getMemusByUserId(Long userId);
}
