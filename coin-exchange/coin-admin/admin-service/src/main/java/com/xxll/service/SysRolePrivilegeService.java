package com.xxll.service;

import com.xxll.domain.SysMenu;
import com.xxll.domain.SysRolePrivilege;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xxll.model.RolePrivilegesParam;

import java.util.List;

public interface SysRolePrivilegeService extends IService<SysRolePrivilege>{


    /**
     * 查询角色的权限
     * @param roleId
     **/
    List<SysMenu> findSysMenuAndPrivileges(long roleId);

    /**
     * 给角色授权
     * @param rolePrivilegesParam
     **/
    boolean grantPrivileges(RolePrivilegesParam rolePrivilegesParam);
}
