package com.xxll.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "authorization-server")
public interface OAuth2FeignClient {

    /**
     * <h2>TODO</h2>
     * @param grantType  授权类型
     * @param username   用户名
     * @param password   用户的密码
     * @param loginType  登录的类型
     * @param basicToken Basic Y29pbi1hcGk6Y29pbi1zZWNyZXQ= 由第三方客户端信息加密出现的值, 如【coin-api和coin-secret】
     **/
    @PostMapping(value = "/oauth/token")
    ResponseEntity<JwtToken> getToken(
            @RequestParam("grant_type") String grantType ,
            @RequestParam("username") String username ,
            @RequestParam("password") String  password ,
            @RequestParam("login_type")  String loginType,
            @RequestHeader("Authorization") String basicToken
    );

}
