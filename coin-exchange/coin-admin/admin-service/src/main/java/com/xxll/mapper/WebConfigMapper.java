package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.WebConfig;

public interface WebConfigMapper extends BaseMapper<WebConfig> {
}