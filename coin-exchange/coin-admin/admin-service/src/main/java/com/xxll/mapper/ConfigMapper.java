package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.Config;

public interface ConfigMapper extends BaseMapper<Config> {
}